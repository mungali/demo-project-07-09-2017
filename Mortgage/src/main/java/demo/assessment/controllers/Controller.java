package  demo.assessment.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import demo.assessment.models.CustomerData;
import demo.assessment.models.MortgageRate;

/**
 * Created by yogesh mungali   
 * We would like you to create a java based backend application using REST.
 *
 */
@RestController
public class Controller {
	
	
	 /*@Value("${demo.microservice.basepath}")
	    private String basePath;

	 @Value("${local.server.port}")
	 private int port;*/
	    
	/**
	 * Create New 
     * POST /api/mortgage-check (post the parameters to calculate for a mortgage check)
	 * @param product
	 * @return
	 */		
	@RequestMapping(value = "/api/mortgage-check", method = RequestMethod.POST)
	 public  @ResponseBody CustomerData mortgageCheck(@RequestBody CustomerData data) {		
	  
	  final String uri = "http://localhost:8080/api/interest-rates?maturityPeriod="+ data.getMaturityPeriod();
	     
	  RestTemplate restTemplate = new RestTemplate();
	  MortgageRate mortgageRate = restTemplate.getForObject(uri, MortgageRate.class);
		  
      //double principal = 200000; 
      double principal = data.getLoanValue(); 
 
      // Annual interest rate
      double rate =  mortgageRate.getInterestRate();
 
      // Monthly intertest rate
      rate = rate/100/12; 
 
      // Term in years
      double term = mortgageRate.getMaturityPeriod();
 
      // Term in months
      term = term * 12; 
 
      double payment = (principal * rate) / (1 - Math.pow(1 + rate, -term));
 
      // round to two decimals
      payment = (double)Math.round(payment * 100) / 100;
      
      
	  /*
	   * Evaluate mortgage eligibility for customer
	   * Business rules that apply are
	   * a mortgage should not exceed 4 times the income
	   * a mortgage should not exceed the home value
	  */	  
     if(data.IsEligible(data))
     {
    	 data.setFeasible(true);
    	 data.setCosts(payment);
     }
     else
     {
    	 data.setFeasible(false);
     }
    	 
	  
	   
	  return data; 
	  
	 }
	
	/**
	 * get a list of current interest rates
	 * @return
	 */
	@RequestMapping(value = "/api/interest-rates", method = RequestMethod.GET)
	public @ResponseBody MortgageRate getMortgageRateObject(@RequestParam("maturityPeriod") double maturityPeriod) {
		
		MortgageRate mortgageRate = new MortgageRate();
	
		mortgageRate.setInterestRate(6.8);	 
		mortgageRate.setMaturityPeriod(maturityPeriod);

		return mortgageRate;

	}
	
}
