package demo.assessment.models;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 
 * @author Yogesh Mungali
 * 
 * The mortgage rate object contains the fields;
 * maturityPeriod (integer), 
 * interestRate (Percentage) 
 * lastUpdate (Timestamp)
 * The posted data for the mortgage check contains at least the fields; 
 * income (Amount), maturityPeriod (integer), loanValue (Amount), homeValue (Amount).
 * The mortgage check return if the mortgage is feasible (boolean) and the monthly 
 * costs (Amount) of the mortgage.	
 *
 */

public class MortgageRate {
	
	private double maturityPeriod;
	private double interestRate;	
	private Timestamp lastUpdate;
	
	public double getMaturityPeriod() {
		return maturityPeriod;
	}

	public void setMaturityPeriod(double maturityPeriod) {
		this.maturityPeriod = maturityPeriod;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public Timestamp getLastUpdate() {
		Date date = new Date();
		lastUpdate = new Timestamp(date.getTime());
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
	
	 

}
