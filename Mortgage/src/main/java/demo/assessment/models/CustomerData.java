package demo.assessment.models;

import java.sql.Timestamp;

/**
 * 
 * @author Yogesh Mungali
 * 
 * The posted data for the mortgage check contains at least the fields; 
 * income (Amount), maturityPeriod (integer), loanValue (Amount), homeValue (Amount).
 * The mortgage check return if the mortgage is feasible (boolean) and the monthly 
 * costs (Amount) of the mortgage.	
 *
 */

public class CustomerData {
	private int maturityPeriod;
	private double income;
	private double loanValue;
	private double homeValue;
	private double costs;
	private boolean feasible;
	private Timestamp lastUpdate;
	
	public boolean IsEligible(CustomerData cust)
	{
		cust.feasible = true; 
		
		 if(cust.getLoanValue() > 4 * cust.getIncome())
			 cust.feasible =  false;
		 
		 if(cust.getLoanValue() > 4 * cust.getHomeValue())
			 cust.feasible = false;
	    	 
	    	 
	      return cust.feasible; 
		
	}
	
	public int getMaturityPeriod() {
		return maturityPeriod;
	}
	public void setMaturityPeriod(int maturityPeriod) {
		this.maturityPeriod = maturityPeriod;
	}
	public double getIncome() {
		return income;
	}
	public void setIncome(double income) {
		this.income = income;
	}
	public double getLoanValue() {
		return loanValue;
	}
	public void setLoanValue(double loanValue) {
		this.loanValue = loanValue;
	}
	public double getHomeValue() {
		return homeValue;
	}
	public void setHomeValue(double homeValue) {
		this.homeValue = homeValue;
	}
	public double getCosts() {
		return costs;
	}
	public void setCosts(double costs) {
		this.costs = costs;
	}
	public boolean isFeasible() {
		return feasible;
	}
	public void setFeasible(boolean feasible) {
		this.feasible = feasible;
	}
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	 

}
